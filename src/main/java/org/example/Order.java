package org.example;

import static java.util.Objects.requireNonNull;

public class Order {

    private final Drink drink;
    private final Integer sugarNumber;
    private final DrinkMaker drinkMaker;

    public Order(DrinkType drink, Integer sugarNumber, DrinkMaker drinkMaker) {
        this.drink = requireNonNull(drink);
        this.sugarNumber = sugarNumber;
        this.drinkMaker = drinkMaker;
    }

    public String send() {
        return drinkMaker.make(getOrder());
    }

    public String getOrder() {
        return drink.getName() + ":" + getSugarNumber() + ":" + getStirrer();
    }

    public String getSugarNumber() {
        return (sugarNumber == null) ? "" : sugarNumber.toString();
    }

    public String getStirrer() {
        return (sugarNumber == null) ? "" : "0";
    }
}
