package org.example;

public class Coffee extends AbstractDrink {

    public Coffee() {
        this("C");
    }

    public Coffee(String name) {
        super(name);
    }

}
