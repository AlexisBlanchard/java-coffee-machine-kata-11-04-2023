package org.example;

public abstract class AbstractDrink implements Drink {
    private String name;

    protected AbstractDrink(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
