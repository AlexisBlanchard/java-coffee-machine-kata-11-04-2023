package org.example;


public class App {

    public static void main(String[] args) {
        Order order = new Order(DrinkType.COFFEE, 2, new DrinkMaker() {
            @Override
            public String make(String order) {
                return null;
            }
        });
        System.out.println(order.send());
    }

}
