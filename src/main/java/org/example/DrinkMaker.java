package org.example;

public interface DrinkMaker {

    String make(String order);

}
