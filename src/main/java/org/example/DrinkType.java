package org.example;

/**
 *
 * @author Alassani ABODJI <abodjialassani[at]gmail.com>
 */
public enum DrinkType implements Drink{
    COFFEE("C"),
    CHOCOLATE("H"),
    THEA("T");

    private String name;

    private DrinkType(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }        
}
