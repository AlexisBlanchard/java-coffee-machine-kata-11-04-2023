package org.example;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 *
 * @author Alassani ABODJI <abodjialassani[at]gmail.com>
 */

@ExtendWith(MockitoExtension.class)
public class OrderTest {

    @Mock
    private DrinkMaker drinkMaker;

    @Test
    public void testSendWithTheaSugarStirrer() {
        Order order = new Order(DrinkType.THEA, 1, drinkMaker);
        String expectedResult = "T:1:0";
        given(drinkMaker.make(expectedResult)).willReturn(expectedResult);

        String actualResult = order.send();

        verify(drinkMaker).make(expectedResult);
    }
    
    @Test
    public void testSendWithChocolateWithNoSugarNoStirrer() {
        Order order = new Order(DrinkType.CHOCOLATE, null, drinkMaker);
        String expectedResult = "H::";
        String actualResult = order.send();

        assertEquals(expectedResult, actualResult);
    }
    
    @Test
    public void testSendWithCoffeeWithTwoSugarAndStirrer() {
        Order order = new Order(DrinkType.COFFEE, 2, drinkMaker);
        String expectedResult = "C:2:0";
        String actualResult = order.send();

        assertEquals(expectedResult, actualResult);
    }
}
